﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Boilr.Core
{
    public class Boilr:IBoilr
    {
        protected readonly ICommandRepository CommandRepo;

        public Boilr(ICommandRepository commandRepo)
        {
            CommandRepo = commandRepo;
        }

        public string Execute(string instructions)
        {
            var instructionElements = instructions.Split(' ');
            var commandName = instructionElements[0];
            var args = instructions.Replace(commandName + " ", string.Empty);
            return CommandRepo.GetCommand(commandName).Execute(args);
        }


    }
}

