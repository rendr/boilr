﻿using System;
using System.Collections.Generic;

namespace Boilr.Core
{
    public interface IBoilr
    {
        string Execute(string instructions);
    }
}

