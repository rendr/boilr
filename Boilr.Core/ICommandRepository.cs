﻿using System;

namespace Boilr.Core
{
    public interface ICommandRepository
    {
        ICommand GetCommand(string commandName);
    }
}

