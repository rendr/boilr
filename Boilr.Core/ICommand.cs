﻿using System;

namespace Boilr.Core
{
    public interface ICommand
    {
        string Execute(string args);
    }
}

