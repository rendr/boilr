﻿using NUnit.Framework;
using System;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace Boilr.Core.Test
{
    [TestFixture()]
    public class BoilrTest
    {
        protected ICommandRepository FakeCommandRepo;

        protected ICommand TestCommand;

        protected Boilr BoilrUT;

        const string testNameOfCommand = "ACommand";


        [Test]
        public void ExecuteCommandWithNoArgs()
        {
            //act
            BoilrUT.Execute(testNameOfCommand);

            //assert
            CommandRepoGet(testNameOfCommand).MustHaveHappened();

        }

        [Test]
        public void ExecuteCommandWithArgs()
        {
            //arrange
            var commandInstructions = string.Format("{0} anArgument", testNameOfCommand);
            //act
            BoilrUT.Execute(commandInstructions);
        
            //assert
            CommandRepoGet(testNameOfCommand).MustHaveHappened();
        
        }

        [Test]
        public void PassArgsToCommand()
        {
            //arrange
            const string arguments = "anArgument anotherArg";
            var commandInstructions = string.Format("{0} {1}", testNameOfCommand, arguments);

            CommandRepoGet(testNameOfCommand).Returns(TestCommand);

            //act
            BoilrUT.Execute(commandInstructions);
            //assert
            CommandExecute(arguments).MustHaveHappened();

        }




        [SetUp]
        public void Init()
        {
            FakeCommandRepo = A.Fake<ICommandRepository>();
            TestCommand = A.Fake<ICommand>();
            BoilrUT = new Boilr(FakeCommandRepo);
        }



        IReturnValueArgumentValidationConfiguration<ICommand> CommandRepoGet(string commandName)
        {
            return A.CallTo(() => FakeCommandRepo.GetCommand(A<string>.That.Matches(s => s == commandName)));
        }

        IReturnValueArgumentValidationConfiguration<string> CommandExecute(string commandInstructions)
        {
            return A.CallTo(() => TestCommand.Execute(A<string>.That.Matches(s => s == commandInstructions)));
        }


        static string StringMatching(string str)
        {
            return A<string>.That.Matches(s => s == str);
        }
    }
}

