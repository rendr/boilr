﻿using NUnit.Framework;
using System;
using Boilr.Core;
using Should;


namespace Boilr.Core.Commands.Tests
{
    [TestFixture]
    public class CommandRepositoryTest
    {
        [Test]
        public void Get()
        {
            //arrange
            var CommandRepoUT = new CommandRepository(new ICommand[]
                {
                    new SomeCommand()
                });
            //act
            var command = CommandRepoUT.GetCommand("SomeCommand");
            //asset
            command.ShouldNotBeNull();
            command.ShouldBeType<SomeCommand>();

        }

        [Test]
        public void GetException()
        {
            //arrange
            var CommandRepoUT = new CommandRepository();
            //act
            try
            {
                CommandRepoUT.GetCommand("SomeCommand");
            }
            catch (Exception ex)
            {
                ex.ShouldBeType<UnkownCommandException>();
            }
        }
    }

    public class SomeCommand:ICommand
    {
        #region ICommand implementation

        public string Execute(string args)
        {
            throw new NotImplementedException();
        }

        #endregion


    }
}

