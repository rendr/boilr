using System;
using System.Collections.Generic;

namespace Boilr.Core.Commands
{

    public class UnkownCommandException:CommandException
    {
        public UnkownCommandException(string commandName)
            : base(commandName)
        {
            
        }

        public override string Message
        {
            get
            {
                return string.Format("{0} is an unkown command", CommandName);
            }
        }
    }
}
