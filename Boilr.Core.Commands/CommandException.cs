using System;
using System.Collections.Generic;

namespace Boilr.Core.Commands
{

    public class CommandException:Exception
    {
        protected readonly string CommandName;

        public CommandException(string commandName)
        {
            CommandName = commandName;
        }
    }
    
}
