﻿using System;
using System.Collections.Generic;

namespace Boilr.Core.Commands
{
    public class CommandRepository:ICommandRepository
    {
        protected Dictionary<string, ICommand> CommandMap = new Dictionary<string, ICommand>();



        public CommandRepository()
        {
    
        }

        public CommandRepository(ICommand[] commands)
        {
            foreach (var command in commands)
            {
                CommandMap[command.GetType().Name] = command;
            }
        }

        #region ICommandRepository implementation

        public ICommand GetCommand(string commandName)
        {
            if (CommandMap.ContainsKey(commandName))
            {
                return CommandMap[commandName];
            }
            throw(new UnkownCommandException(commandName));
        }

        #endregion
    }


}

